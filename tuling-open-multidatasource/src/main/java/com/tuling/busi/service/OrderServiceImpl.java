package com.tuling.busi.service;

import com.tuling.busi.bean.Order;
import com.tuling.busi.dao.OrderMapper;
import com.tuling.busi.dao.UserMapper;
import com.tuling.multidatasource.annotation.Router;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * Created by 爆裂无球 on 2019/4/17.
 */
@Service
public class OrderServiceImpl {

    @Autowired
    private OrderMapper orderMapper;
    @Autowired
    private UserMapper userMapper;


    public void insertOrder(Order order){
        orderMapper.insertOrder(order);
    }

    @Router(routingFiled = "orderId")
    public void insertOrder3(Order order){

        orderMapper.insertOrder(order);
    }



    public void insertS(Map<String,Object> params){
        orderMapper.insertS(params);
    }



    public List<Order> getByOrderId(Long orderId,String tableSuffix){

        return orderMapper.getByOrderId(orderId,tableSuffix);
    }
}
