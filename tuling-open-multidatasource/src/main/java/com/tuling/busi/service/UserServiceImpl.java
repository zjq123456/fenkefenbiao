package com.tuling.busi.service;

import com.tuling.busi.bean.User;
import com.tuling.busi.dao.UserMapper;
import com.tuling.multidatasource.annotation.Router;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Auther: 爆裂无球
 * @Date: 2019/4/22 13:56
 * @Description:
 */
@Service
public class UserServiceImpl {

    @Autowired
    private UserMapper userMapper;


    @Router(routingFiled = "userId")
    public void insert(User user) {
        userMapper.insertUser(user);
    }


}
