package com.tuling.busi.controller;

import com.tuling.busi.bean.Order;
import com.tuling.busi.bean.User;
import com.tuling.busi.service.UserServiceImpl;
import com.tuling.multidatasource.annotation.Router;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Auther: 爆裂无球
 * @Date: 2019/4/22 13:54
 * @Description:
 */
@RestController
@RequestMapping("user")
public class UserController {


    @Autowired
    private UserServiceImpl userService;


    /**
     * 新增
     *
     * @param user
     * @return
     */
    @RequestMapping("/save")
    @Router(routingFiled = "userId")
    public void insertUser(User user) {
        userService.insert(user);
    }


}
