package com.tuling.busi.dao;

import com.tuling.busi.bean.Order;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.aspectj.weaver.ast.Or;

import java.util.List;
import java.util.Map;

/**
 * Created by 爆裂无球 on 2019/4/17.
 */
public interface OrderMapper {

    void insertOrder(Order order);


    @Insert("insert into student values(#{params.id},#{params.name})")
    void insertS(@Param("params")Map<String,Object> params);


    List<Order> getByOrderId(@Param("orderId") Long orderId,@Param("tableSuffix") String tableSuffix);
}
