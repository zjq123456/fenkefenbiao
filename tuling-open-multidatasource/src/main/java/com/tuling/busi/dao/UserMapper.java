package com.tuling.busi.dao;

import com.tuling.busi.bean.Order;
import com.tuling.busi.bean.User;
import org.apache.ibatis.annotations.Insert;

/**
 * @Auther: 爆裂无球
 * @Date: 2019/4/22 13:57
 * @Description:
 */
public interface UserMapper {


    @Insert(" insert into user${tableSuffix}(user_id,order_id,money) values(#{userId},#{orderId},#{money}) ")
    void insertUser(User user);


}
