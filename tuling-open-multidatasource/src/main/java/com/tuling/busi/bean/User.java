package com.tuling.busi.bean;

import lombok.Data;

/**
 * @Auther: 爆裂无球
 * @Date: 2019/4/22 13:55
 * @Description:
 */
@Data
public class User extends BaseDomin {
    private Long userId;

    private Long orderId;


    private double money;

}
